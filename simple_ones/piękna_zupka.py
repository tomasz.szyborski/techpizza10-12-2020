# docs: https://www.crummy.com/software/BeautifulSoup/bs4/doc/
from bs4 import BeautifulSoup
import requests as r
import os

url = "http://lublin.eu"
html_document = r.get(url).content
# html_document = r.get(url=url).content
print(html_document)

soup = BeautifulSoup(html_document, 'html.parser')

print(soup.prettify())

if not os.path.exists("fotki"):
    os.mkdir("fotki")

img_webelement_list = soup.find_all("img")

for element in img_webelement_list:
    response = r.get(f"{url}{element['src']}")
    try:
        nazwa_pliku = element['src'].split("/")[-1]
        print(f"Ściągam zdjęcie pod tytułęm: {nazwa_pliku}")
        response.raise_for_status()
        # print(response.headers)
        # print(response.content)
        # nazwa_pliku = element['src'][-15:]

        with open(f"fotki/{nazwa_pliku}", "wb") as file:
            file.write(response.content)
    except:
        print(f"Smuteczeq :( ")
