from __future__ import print_function, unicode_literals
import requests as r
import os

animal = "cat"

if animal == "cat":
    url = "https://http.cat/"
elif animal == "dog":
    url = "https://httpstatusdogs.com/img/"
else:
    raise NotImplementedError


# if not os.path.exists(animal):
#     os.mkdir(animal)

def make_me_a_directory(path):
    if not os.path.exists(path):
        os.mkdir(path)


make_me_a_directory(animal)

for code in range(100, 600):
    resp = r.get(f"{url}{code}.jpg")
    try:
        resp.raise_for_status()
        # print(resp.headers)
        # print(resp.content)
        with open(f"{animal}/{animal}_{code}.jpg", "wb") as file:
            file.write(resp.content)
    except:
        print(f"Nie mamy pana płaszcza nr {code} i co nam pan zrobi.")
